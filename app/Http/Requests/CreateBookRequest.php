<?php

namespace App\Http\Requests;

use App\Models\Book;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Book::ISBN => 'required|unique:books,isbn',
            Book::TITLE => 'required|string',
            Book::AUTHOR => 'required|string',
            Book::PRICE => 'required|between:0,999.99',
            Book::RELATION_CATEGORY => 'required|string|exists:category,name'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                $validator->errors(), 400)
        );
    }

    public function messages()
    {
        return [
            Book::ISBN.'.unique' => 'Invalid ISBN'
        ];
    }
}
