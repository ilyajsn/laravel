<?php

namespace App\Http\Controllers\Category\Get;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\UseCase\Category\Get\Contracts\GetCategoryCase;

class GetCategoryController extends Controller
{
    /** @var GetCategoryCase $case */
    private $case;

    /**
     * GetCategoryController constructor.
     * @param GetCategoryCase $case
     */
    public function __construct(GetCategoryCase $case)
    {
        $this->case = $case;
    }

    public function __invoke()
    {
        $categories = $this->case->execute();

        return response()->json($categories->pluck(Category::NAME));
    }
}
