<?php

namespace App\Http\Controllers\Books\Get;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Repository\Book\Contracts\BookRepository;
use App\UseCase\Books\Get\Contracts\GetBooksCase;
use Illuminate\Http\Request;

class GetBooksController extends Controller
{
    private $case;

    public function __construct(GetBooksCase $case)
    {
        $this->case = $case;
    }

    public function __invoke(Request $request)
    {
        $books = $this->case->execute($request);

        return response()->json($books->pluck(Book::ISBN));
    }
}
