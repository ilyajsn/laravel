<?php

namespace App\Http\Controllers\Books\Create;

use App\Http\Requests\CreateBookRequest;
use App\UseCase\Books\Create\Contracts\CreateBookCase;

class CreateBookController
{
    private $case;

    public function __construct(CreateBookCase $case)
    {
        $this->case = $case;
    }

    public function __invoke(CreateBookRequest $request)
    {
        $book = $this->case->execute($request);

        return response()->json($book, 201);
    }
}
