<?php

namespace App\Repository\Book;

use App\Models\Book;
use App\Models\Category;
use App\Repository\Book\Contracts\BookRepository as BookRepositoryContract;
use Illuminate\Database\Eloquent\Collection;

class BookRepository implements BookRepositoryContract
{
    /** @var Book $model */
    private $model;

    /**
     * BookRepository constructor.
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        $this->model = $book;
    }

    /**
     * @return Collection
     */
    public function getAllWithCategory(): Collection
    {
        return $this->model->with(Book::RELATION_CATEGORY)->get();
    }

    /**
     * @param array $request
     * @return Book
     */
    public function create(array $request): Book
    {
       return $this->model->create($request);
    }
}
