<?php

namespace App\Repository\Book\Contracts;

use App\Models\Book;
use Illuminate\Database\Eloquent\Collection;

interface BookRepository
{
    /**
     * @return Collection
     */
    public function getAllWithCategory(): Collection;

    /**
     * @param array $request
     * @return Book
     */
    public function create(array $request): Book;
}
