<?php

namespace App\Repository\Category;

use App\Models\Category;
use App\Repository\Category\Contracts\CategoryRepository as CategoryRepositoryContract;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository implements CategoryRepositoryContract
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function findByName(string $name): Category
    {
        return $this->category->where(Category::NAME, $name)->firstOrFail();
    }

    public function get(): Collection
    {
        return $this->category->all();
    }
}
