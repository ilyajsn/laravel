<?php

namespace App\Repository\Category\Contracts;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepository
{
    public function get(): Collection;

    public function findByName(string $name): Category;
}
