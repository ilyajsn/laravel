<?php

namespace App\Providers;

use App\UseCase\Books\Create\Contracts\CreateBookCase as CreateBookCaseContract;
use App\UseCase\Books\Create\CreateBookCase;
use App\UseCase\Books\Get\Contracts\GetBooksCase as GetBooksCaseContract;
use App\UseCase\Books\Get\GetBooksCase;
use App\UseCase\Category\Get\Contracts\GetCategoryCase as GetCategoryCaseContract;
use App\UseCase\Category\Get\GetCategoryCase;
use Illuminate\Support\ServiceProvider;

class UseCaseProvider extends ServiceProvider
{
    public function register()
    {
        /** Book */
        $this->app->bind(GetBooksCaseContract::class, GetBooksCase::class);
        $this->app->bind(CreateBookCaseContract::class, CreateBookCase::class);
        /** Category */
        $this->app->bind(GetCategoryCaseContract::class, GetCategoryCase::class);
    }
}
