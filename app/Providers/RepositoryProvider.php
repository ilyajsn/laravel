<?php

namespace App\Providers;

use App\Repository\Book\BookRepository;
use App\Repository\Book\Contracts\BookRepository as BookRepositoryContract;
use App\Repository\Category\CategoryRepository;
use App\Repository\Category\Contracts\CategoryRepository as CategoryRepositoryContract;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    public function register()
    {
        /** Book */
        $this->app->bind(BookRepositoryContract::class, BookRepository::class);
        /** Category */
        $this->app->bind(CategoryRepositoryContract::class, CategoryRepository::class);
    }
}
