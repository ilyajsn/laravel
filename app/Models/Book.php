<?php

namespace App\Models;

use Database\Factories\BooksFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method filter() as scopeFilter
 */
class Book extends Model
{
    use HasFactory;

    const ID = 'id';
    const ISBN = 'isbn';
    const TITLE = 'title';
    const AUTHOR = 'author';
    const PRICE = 'price';

    const RELATION_CATEGORY = 'category';

    protected $table = 'books';

    protected $fillable = [
        self::ISBN,
        self::TITLE,
        self::AUTHOR,
        self::PRICE
    ];

    public $timestamps = false;

    public function category()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function checkCategory(string $name): bool
    {
        return $this->getRelation(self::RELATION_CATEGORY)->where(Category::NAME, $name)->isNotEmpty();
    }

    public function getAuthor(): string
    {
        return $this->getAttributeValue(self::AUTHOR);
    }
}
