<?php

namespace App\Models;

use Database\Factories\CategoryFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'category';

    const NAME = 'name';

    protected $fillable = [
        self::NAME
    ];


    public $timestamps = false;

    public function books()
    {
        $this->belongsToMany(Book::class, 'book_category', 'category_id', 'book_id');
    }
}
