<?php

namespace App\UseCase\Books\Create;

use App\Http\Requests\CreateBookRequest;
use App\Models\Book;
use App\Repository\Book\Contracts\BookRepository;
use App\Repository\Category\Contracts\CategoryRepository;
use App\UseCase\Books\Create\Contracts\CreateBookCase as CreateBookCaseContract;

class CreateBookCase implements CreateBookCaseContract
{
    /** @var BookRepository $bookRepository */
    private $bookRepository;

    /** @var CategoryRepository $categoryRepository */
    private $categoryRepository;

    public function __construct(BookRepository $bookRepository, CategoryRepository $categoryRepository)
    {
        $this->bookRepository = $bookRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(CreateBookRequest $request): Book
    {
        $book = $this->bookRepository->create($request->toArray());
        $category = $this->categoryRepository->findByName($request->get('category'));
        $book->category()->attach($category);

        return $book;
    }
}
