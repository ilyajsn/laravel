<?php


namespace App\UseCase\Books\Create\Contracts;

use App\Http\Requests\CreateBookRequest;
use App\Models\Book;

interface CreateBookCase
{
    public function execute(CreateBookRequest $request): Book;
}
