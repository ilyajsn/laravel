<?php

namespace App\UseCase\Books\Get\Contracts;

use Illuminate\Http\Request;

interface GetBooksCase
{
    public function execute(Request $request);
}
