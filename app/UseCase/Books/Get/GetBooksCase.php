<?php

namespace App\UseCase\Books\Get;

use App\Models\Book;
use App\Models\Category;
use App\Repository\Book\Contracts\BookRepository;
use App\UseCase\Books\Get\Contracts\GetBooksCase as GetBooksCaseContract;
use Illuminate\Http\Request;

class GetBooksCase implements GetBooksCaseContract
{
    const AUTHOR = 'author';
    const CATEGORY = 'category';

    private $bookRepository;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function execute(Request $request)
    {
        $books = $this->bookRepository->getAllWithCategory();

        if ($request->get(self::AUTHOR)) {
            $books = $books->filter(function ($book) use ($request) {
                /** @var Book $book */
                return $book->getAuthor() == $request->get(self::AUTHOR);
            });
        }

        if ($request->get(self::CATEGORY)) {
            $books = $books->filter(function ($book) use ($request) {
                /** @var Book $book */
                return $book->checkCategory($request->get(self::CATEGORY));
            });
        }

        return $books;
    }
}
