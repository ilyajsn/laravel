<?php

namespace App\UseCase\Category\Get;

use App\Repository\Category\Contracts\CategoryRepository;
use App\UseCase\Category\Get\Contracts\GetCategoryCase as GetCategoryCaseContract;
use Illuminate\Database\Eloquent\Collection;

class GetCategoryCase implements GetCategoryCaseContract
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(): Collection
    {
        return $this->categoryRepository->get();
    }
}
