<?php

namespace App\UseCase\Category\Get\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface GetCategoryCase
{
    public function execute(): Collection;
}
